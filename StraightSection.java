
package programmingproject7;

import java.util.ArrayList;

/**class for a row in a sudoku board
 */
public class StraightSection implements Section
{
    //private int rowNum;
    private Square[] squares;
    private int numSquares;
    
    public StraightSection()
    {
        squares = new Square[9];
        numSquares = 0;
    }
    
    /*method to find if a there is a value in the section that only appears in 1
    square's possible solutions
    pre: assumes there is a hidden single(determined in a different method) 
    also takes in the value that is the hidden single
    post: return the square that contains the hidden single number*/
    public Square getHiddenSingle(int value)
    {
        Square hiddenSingle;//square to return
        int index;//index of the value
        
        for(int i = 0; i < squares.length; i++)
        {
            //if this square's possible solution values contain the hidden single number
            index = squares[i].getPossibleSolutionValues().indexOf(value);
            if(index != -1)
            {
                hiddenSingle = squares[i];
                return hiddenSingle;//return that square
            }
        }
        
        return null;//because netbeans made me
    }
    
    /*method that counts the number of times 1-9 is a possible solution in each square
    for the section
    pre:
    post: returns an int array of size 9 with each index holding the number of times
    its index+1 appears in the possible soltutions*/
    public int[] countPossibilitiesOfSolutionValues()
    {
        int[] numberOfPossibilities = new int[9];//array to return
        
        ArrayList<Integer> possibleValues;
        
        //for each square
        for(int i = 0; i < squares.length; i++)
        {
            if(!squares[i].getVisibility())
            {
                possibleValues = squares[i].getPossibleSolutionValues();//store this squares possible solutionvalues
            
                //for each possible value
                for(int j = 0; j < possibleValues.size(); j++)
                {
                    int value = possibleValues.get(j);
                    numberOfPossibilities[value-1]++;//update appropriate array location count
                }
            }
            
        }
        
        
        return numberOfPossibilities;
    }
    
    /*get method for the squares array
    pre
    post: returns the squares array*/
    public Square[] getSquares()
    {
        return squares;
    }
    
    /*method used for adding preinitialized squares to the squares array
    pre: takes in a square to add
    post: adds square to next squares array position*/
    public void addSquare(Square square)
    {
        squares[numSquares] = square;
        numSquares++;
    }
    
    

    /*method to test if a value is already in this section
    pre: takes in the value to test for
    post: returns false if the value is already in the section
    returns true if it is not*/
    @Override
    public boolean validValueForSection(int value) 
    {
        for(int i = 0; i < squares.length; i++)
        {
            if(value == squares[i].getValue())
                return false;
        }
        
        return true;    
    }

    /*method to test if a value is already in this solution section
    pre: takes in the value to test for
    post: returns false if the value is already in the section of the solution
    returns true if it is not*/
    @Override
    public boolean validValueForSolutionSection(int value) 
    {
        for(int i = 0; i < squares.length; i++)
        {
            if(value == squares[i].getSolutionValue())
                return false;
        }
        
        return true;
    }
        
}
