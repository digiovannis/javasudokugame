
package programmingproject7;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

/**Class for a sudoku game
 */
public class Sudoku extends JFrame
{
    private Square[][] board;//all of the squares that make up the sudoku game
    
    
    private StraightSection[] rows;//all of the rows in the game
    private StraightSection[] columns;//all of the columns in the game
    private Region[] regions;//all of the 3x3 regions in the game
    
    private final int sudokuSize = 9;//size of this sudoku
    
    private JButton generateNewGameButton;//button to create a new sudoku puzzle
    private JButton checkSolutionButton;//button to check the users solution to puzzle
    
    //buttons to set the value in a blank sudoku cell
    private JButton[] numberButtons;
    
    private JTextField[][] cells;//all of the cells in the sudoku game
    
    private BlankCell selectedCell;//the cell that the user selects
    
    private BlankCellHandler blankCellHandler;//focus handler for all blank sudoku cells
    
    private JPanel sudokuPanel;//the panel that holds all the sudoku cells
    
    /*construtor for the sudoku game.
    it creates the solution board, the puzzle board, and gui*/
    public Sudoku()
    {
        super("Sudoku");//set name of window
        
        board = new Square[sudokuSize][sudokuSize];//intialize the board
        
        //intitalize each section array
        regions = new Region[sudokuSize];
        columns = new StraightSection[sudokuSize];
        rows = new StraightSection[sudokuSize];
        
        //initalize all rows columns and regions
        for( int i = 0; i < sudokuSize; i++)
        {
            regions[i] = new Region();
            columns[i] = new StraightSection();
            rows[i] = new StraightSection();
        }
        
        
        initializeSquaresAndRegions();
        initializeRowsAndColumns();
        
        generateSolutionBoard();
        generateBlankSquares();
        resetSolutionValues();
        generateGUI();
    }
    
    
    /*method that contains the code to generate the gui
    pre: the solution board and puzzle are already generated
    post: creates a gui for a playable sudoku game*/
    public void generateGUI()
    {
        this.setLayout(new GridLayout(1, 2));//set overall layout
        
        blankCellHandler = new BlankCellHandler();//create a focuslistener for the blank cells
        
        //create an action listener for all of the number buttons
        NumberButtonHandler numberButtonHandler = new NumberButtonHandler();
        
        //intialize the cells
        cells = new JTextField[sudokuSize][sudokuSize];
        
        //font to use on buttons
        Font buttonFont = new Font(Font.SERIF, Font.PLAIN, 20);
        
        JPanel buttonPanel = new JPanel();//panel for the buttons
        sudokuPanel = new JPanel();//initalize the sudoku panel
        sudokuPanel.setLayout(new GridLayout(3, 3));//set a 3x3 layout for the panel
        
        generateNewGameButton = new JButton("New Game");//initialize new game button
        generateNewGameButton.setFont(buttonFont);//set font
        //add its action listener for button functionality
        generateNewGameButton.addActionListener(new NewGameButtonHandler());
        buttonPanel.add(generateNewGameButton);//add button to appropriate panel
        
        checkSolutionButton = new JButton("Check Solution");//initialize check solution button
        checkSolutionButton.setFont(buttonFont);//set font
        //add its action listener for button functionality
        checkSolutionButton.addActionListener(new CheckSolutionButtonHandler());
        buttonPanel.add(checkSolutionButton);//add to button panel
        
        //create box for all the number buttons
        Box numberButtonBox = Box.createHorizontalBox();
        
        //initalize all buttons and add the actionlistener, set font, and add to box
        numberButtons = new JButton[9];
        for(int i = 0; i < 9; i++)
        {
            numberButtons[i] = new JButton(Integer.toString(i + 1)); 
            numberButtons[i].addActionListener(numberButtonHandler);
            numberButtons[i].setFont(buttonFont);
            numberButtonBox.add(numberButtons[i]);
        }
        
        
        
        //add box to button panel
        buttonPanel.add(numberButtonBox);
        
        JPanel[] regionPanels = new JPanel[9];//create nine panels for the regions of the board
        for(int i = 0; i < 9; i++)
        {
            regionPanels[i] = new JPanel();//initialize panel
            regionPanels[i].setLayout(new GridLayout(3, 3));//set 3x3 layout
            regionPanels[i].setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));//create a black boarder to define each region
        }
        
        //create a border to be shared among all cells
        Border cellBorder = BorderFactory.createLineBorder(Color.BLACK, 1);
        
        //initailize all cells 
        for(int r = 0; r < sudokuSize; r++)
        {
            for(int c = 0; c < sudokuSize; c++)
            {
                if(board[r][c].getVisibility())//if it is not supposed to be a blank cell
                {
                    cells[r][c] = new NumberCell(board[r][c]);//initialize it to be a number cell
                    cells[r][c].setBorder(cellBorder);
                }
                else//if it is supposed to be a blank cell
                {
                    cells[r][c] = new BlankCell(board[r][c]);//initialize it to be a blank cell
                    cells[r][c].setBorder(cellBorder);
                    cells[r][c].addFocusListener(blankCellHandler);//add the focus listener to it
                }
            }
        }
        
        //add each cell to appropriate region panel
        for(int r = 0; r < 3; r++)
        {
            for(int c = 0; c < 3; c++)
            {
                regionPanels[0].add(cells[r][c]);
            }
            for(int c = 3; c < 6; c++)
            {
                regionPanels[1].add(cells[r][c]);
            }
            for(int c = 6; c < 9; c++)
            {
                regionPanels[2].add(cells[r][c]);
            }
        }
        for(int r = 3; r < 6; r++)
        {
            for(int c = 0; c < 3; c++)
            {
                regionPanels[3].add(cells[r][c]);
            }
            for(int c = 3; c < 6; c++)
            {
                regionPanels[4].add(cells[r][c]);
            }
            for(int c = 6; c < 9; c++)
            {
                regionPanels[5].add(cells[r][c]);
            }
        }
        for(int r = 6; r < 9; r++)
        {
            for(int c = 0; c < 3; c++)
            {
                regionPanels[6].add(cells[r][c]);
            }
            for(int c = 3; c < 6; c++)
            {
                regionPanels[7].add(cells[r][c]);
            }
            for(int c = 6; c < 9; c++)
            {
                regionPanels[8].add(cells[r][c]);
            }
        }
        
        //add region panels to sudoku panel
        for(int i = 0; i < regionPanels.length; i++)
        {
            sudokuPanel.add(regionPanels[i]);
        }
        
        this.add(sudokuPanel);//add sudoku panel to frame
        this.add(buttonPanel);//add button panel to frame
    }
    
    
    /*Inner clase for the check solution button action listener*/
    private class CheckSolutionButtonHandler implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e) 
        {
            int incorrectCells = 0;//variable to store the number of incorrect user guesses
            int cellValue;//variable to store the value the user put in a blank cell
            
            //for all of the cells
            for(int r = 0; r < sudokuSize; r++)
            {
                for(int c = 0; c < sudokuSize; c++)
                {
                    if(cells[r][c] instanceof BlankCell)//if it is a blank cell
                    {
                        try
                        {
                            cellValue = Integer.parseInt(cells[r][c].getText());//get the value in the cell
                            if(cellValue != board[r][c].getValue())//see if they match
                            {
                                //if not color it red and update number of incorrect cells
                                cells[r][c].setForeground(Color.red);
                                incorrectCells++;
                            }
                        }
                        catch(NumberFormatException exception)//if there is no value in the cell
                        {
                            //update the number of incorrect cells
                            incorrectCells++;
                        }
                        
                        
                    }
                }
            }
            
            if(incorrectCells == 0)//if the solution is correct
            {
                JOptionPane.showMessageDialog(null, "Congratulation! You win! Press New Game to play again.");
            }
            else//if there are wrong answers or blank cells
            {
                JOptionPane.showMessageDialog(null, "Oops you have some errors, \n"
                        + "Either there are some blank squares or \n"
                        + "the incorrect numbers are marked in red. \n Please try again");
            }
        }
        
    }
    
    /*inner class for the action listener for the new game button*/
    private class NewGameButtonHandler implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e) 
        {
            generateSolutionBoard();//create a new board
            generateBlankSquares();//generate the new puzzle
            resetSolutionValues();//reset the solution values
            
            getContentPane().removeAll();//remove old gui components
            //regenerate gui
            generateGUI();
            revalidate();
            repaint();
        }
        
    }
    
    /*inner class for the action listener for all of the number buttons*/
    public class NumberButtonHandler implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e) 
        {
            
            //find the button that was pressed and set the selected cell
            //to the number of the button and set the text to black to 
            //no longer display in red if it was previously guessed wrong
            
            if(e.getSource() == numberButtons[0])
            {
                selectedCell.setText("1");
                selectedCell.setForeground(Color.BLACK);
            }
            else if(e.getSource() == numberButtons[1])
            {
                selectedCell.setText("2");
                selectedCell.setForeground(Color.BLACK);
            }
            else if(e.getSource() == numberButtons[2])
            {
                selectedCell.setText("3");
                selectedCell.setForeground(Color.BLACK);
            }
            else if(e.getSource() == numberButtons[3])
            {
                selectedCell.setText("4");
                selectedCell.setForeground(Color.BLACK);
            }
            else if(e.getSource() == numberButtons[4])
            {
                selectedCell.setText("5");
                selectedCell.setForeground(Color.BLACK);
            }
            else if(e.getSource() == numberButtons[5])
            {
                selectedCell.setText("6");
                selectedCell.setForeground(Color.BLACK);
            }
            else if(e.getSource() == numberButtons[6])
            {
                selectedCell.setText("7");
                selectedCell.setForeground(Color.BLACK);
            }
            else if(e.getSource() == numberButtons[7])
            {
                selectedCell.setText("8");
                selectedCell.setForeground(Color.BLACK);
            }
            else if(e.getSource() == numberButtons[8])
            {
                selectedCell.setText("9");
                selectedCell.setForeground(Color.BLACK);
            }
            
        }
        
    }
    
    /*inner class for the focus listener for all  of the blankcells*/
    public class BlankCellHandler implements FocusListener
    {

        @Override
        public void focusGained(FocusEvent e) 
        {
            if(selectedCell != null)//if there is a previous selected cell
            {
                selectedCell.setBackground(Color.WHITE);//set that to have a white background again
            }
            selectedCell = (BlankCell)e.getSource();//set this cell to be the new selected cell
            
            selectedCell.setBackground(Color.BLUE);//highlight it by setting its background color to blue
        }

        @Override
        public void focusLost(FocusEvent e) //nothing happens when the focus is lost
        {
        }
        
    }
    
    /*https://www.codeproject.com/articles/23206/sudoku-algorithm-generates-a-valid-sudoku-in
    method for generating a valid complete sudoku board with the algorithm taken from above website
    pre:
    post: generates a valid sudoku board*/
    public void generateSolutionBoard()
    {
        //start
        Random rand = new Random();
        //create random int generator
        //for every square
        for(int r = 0; r < sudokuSize; r++)
        {
            for(int c = 0; c < sudokuSize; c++)
            {
                //are we out of possible values?
                // if yes replenish possible values and backtrack one square
                if(board[r][c].getPossibleValues().isEmpty())
                {
                    board[r][c].replenishPossibleValues();
                    if(c == 0 && r == 0)//if it is the very first square
                    {
                        c = -1;
                        clearBoard();//clear the board and start over
                        
                    }
                    else if(c == 0)//if it is the first in a row
                        
                    {
                        c = 7;
                        r = r - 1;
                    }
                    else
                        c = c - 2;
                }
                else
                {
                    //else get a random number from available numbers
                    int possibleIndex = rand.nextInt(board[r][c].getPossibleValues().size());
                    int value = board[r][c].getPossibleValues().get(possibleIndex);
                    
                    //check to see if it conflicts
                    if(valueConflictsBoard(board[r][c], value))
                    {
                        //if it conflicts remove from possible values and start over
                        board[r][c].getPossibleValues().remove(possibleIndex);
                        
                        if(c == 0 && r == 0)//if it is the very first square
                        {
                            c = -1;            
                        }
                        else if(c == 0)//if it is the first in a row
                        {
                            c = 8;
                            r = r - 1;
                        }
                        else
                            c = c - 1;
                    }
                    else
                    {
                        //else add number to square and move to next square
                        board[r][c].setValue(value);
                        board[r][c].setSolutionValue(value);
                    }
                    
                }
            }
        }
     
    }
    
    
    
    /*https://www.emogic.com/notes/programming_sudoku_algorithm_part_3_seven_basic_ways_solving_sudoku
    method for a sudoku solving strategy with algorithm taken from above website
    pre:
    post: returns true if it filled in atleast one square during its pass through of the board
    returns false if it does not fill in a square*/
    public boolean nakedSinglesSolved()
    {
        boolean solved = false;
        //Naked Singles (NS): Naked Singles iterates through all the squares on the board, 
        for(int r = 0; r < sudokuSize; r++)
        {
            for(int c = 0; c < sudokuSize; c++)
            {
                
                
                Square square = board[r][c];//store square
                
                if(!square.getVisibility())//if it is a blank square
                {
                    //calculating the possible values in each square 
                    ArrayList<Integer> possibleValues = square.getPossibleSolutionValues();//store values
                    possibleValues.clear();//clear in case there are leftover values
                    //for each possible value
                    for(int i = 1; i <=9; i++)
                    {
                        if(!valueConflictsSolutionBoard(square, i))//see if it would conflict with already solved numbers
                        {
                            possibleValues.add(i);//if not add it
                        }
                    }
                    //if a square has no possible values this puzzle is not solvable
                    if(possibleValues.size() == 0)
                        return false;
                    //When we find a cell with a single possibility 
                    if(possibleValues.size() == 1)
                    {
                        //we fill in the Sudoku cell with that value.
                        square.setSolutionValue(possibleValues.get(0));
                        
                        //The $solvable variable is set to 1 (in my case to true) 
                        //if we have at least filled in one blank square 
                        //during the last loop. This at least gives us the hope that this 9 x 9 grid is solvable. 
        
                        solved = true;
                        
                        //Then we remove that possibility from the affected column, row and square regions.
                        removePossibleSolutionValueFromSections(square, possibleValues.get(0));
                        
                    }
                    
                }
                
            }
        }
        return solved;
    }
    
    /*method to remove a just solved value from all affected sections
    pre: takes in the square that was just solved and the value to be removed
    post: that value is removed from the possible solution values for all affected squares*/
    public void removePossibleSolutionValueFromSections(Square square, int value)
    {
        //store the section numbers
        int row = square.getRow();
        int col = square.getColumn();
        int reg = square.getRegion();

        //remove from row and column
        for(int i = 0; i < 9; i++)
        {
            rows[row].getSquares()[i].removePossibleSolutionValue(value);

            columns[col].getSquares()[i].removePossibleSolutionValue(value);

        }
        //remove from region
        for(int j = 0; j < 3; j++)
        {
            for(int k = 0; k < 3; k++)
            {
                regions[reg].getSquares()[j][k].removePossibleSolutionValue(value);
            }
        }
    }
    /*https://www.emogic.com/notes/programming_sudoku_algorithm_part_3_seven_basic_ways_solving_sudoku
    method to for a sudoku solving strategy
    pre:
    post: returns true if it filled in at least one square during its pass through
    returns false if not*/
    public boolean hiddenSinglesSolved()
    {
        boolean solved = false;
        
        int[] numberOfPossibilities;//array to store the number of possiblities of its array index+1
        
        //Hidden Singles (HS): Hidden Singles scan each row, column,
        //and 3 x 3 square for the "number of possibilities" (not the possibilities) 
        //for each number ranging from one to nine. 
        
        //rows
        for(int i = 0; i < rows.length; i++)
        {
            numberOfPossibilities = rows[i].countPossibilitiesOfSolutionValues();
            Square hiddenRowSingle;
            //for each possible value
            for(int j = 0; j < numberOfPossibilities.length; j++)
            {
                //When it finds a cell containing a possibility of a number that appears only once in a row,
                //column or 3 x 3 square, 
                if(numberOfPossibilities[j] == 1)
                {
                    //we fill in the Sudoku board with that value. 
                    hiddenRowSingle = rows[i].getHiddenSingle(j+1);
                    hiddenRowSingle.setSolutionValue(j+1);
                    //The $solvable variable is set to 1 (in my case to true) 
                    //if we have at least filled in one blank square 
                    //during the last loop. This at least gives us the hope that this 9 x 9 grid is solvable. 
        
                    solved = true;
                    
                    
                    //Then we remove that possibility from the affected column, row and square regions.
                    removePossibleSolutionValueFromSections(hiddenRowSingle, j+1);
                    
                }
            }
        }
        //columns
        for(int i = 0; i < columns.length; i++)
        {
            numberOfPossibilities = columns[i].countPossibilitiesOfSolutionValues();
            Square hiddenColumnSingle;
            //for each possible value
            for(int j = 0; j < numberOfPossibilities.length; j++)
            {
                //When it finds a cell containing a possibility of a number that appears only once in a row,
                //column or 3 x 3 square, 
                if(numberOfPossibilities[j] == 1)
                {
                    //we fill in the Sudoku board with that value. 
                    hiddenColumnSingle = columns[i].getHiddenSingle(j+1);
                    hiddenColumnSingle.setSolutionValue(j+1);
                    //The $solvable variable is set to 1 (in my case to true) 
                    //if we have at least filled in one blank square 
                    //during the last loop. This at least gives us the hope that this 9 x 9 grid is solvable. 
        
                    solved = true;
                    
                    
                    //Then we remove that possibility from the affected column, row and square regions.
                    removePossibleSolutionValueFromSections(hiddenColumnSingle, j+1);
                    
                }
            }
        }
        //regions
        for(int i = 0; i < regions.length; i++)
        {
            numberOfPossibilities = regions[i].countPossibilitiesOfSolutionValues();
            Square hiddenRegionSingle;
            //for each possible value
            for(int j = 0; j < numberOfPossibilities.length; j++)
            {
                //When it finds a cell containing a possibility of a number that appears only once in a row,
                //column or 3 x 3 square, 
                if(numberOfPossibilities[j] == 1)
                {
                    //we fill in the Sudoku board with that value. 
                    hiddenRegionSingle = regions[i].getHiddenSingle(j+1);
                    
                    hiddenRegionSingle.setSolutionValue(j+1);
                    
                    //The $solvable variable is set to 1 (in my case to true) 
                    //if we have at least filled in one blank square 
                    //during the last loop. This at least gives us the hope that this 9 x 9 grid is solvable. 
        
                    solved = true;
                    
                    
                    //Then we remove that possibility from the affected column, row and square regions.
                    removePossibleSolutionValueFromSections(hiddenRegionSingle, j+1);
                    
                }
            }
        }
        


        return solved;
    }
    
    
    /*method to determine if a value to be added to a square in the solution is valid
    pre: takes in the square and value in question
    post: returns true if it conflicts and false if it does not*/
    public boolean valueConflictsSolutionBoard(Square square, int value)
    {
        int row = square.getRow();
        int col = square.getColumn();
        int reg = square.getRegion();
        
        return (!rows[row].validValueForSolutionSection(value) || !columns[col].validValueForSolutionSection(value) || !regions[reg].validValueForSolutionSection(value));
    }
    
    /*method to determine if a value to be added to a square is valid
    pre: takes in the square and value in question
    post: returns true if it conflicts and false if it does not*/
    public boolean valueConflictsBoard(Square square, int value)
    {
        int row = square.getRow();
        int col = square.getColumn();
        int reg = square.getRegion();
        
        return (!rows[row].validValueForSection(value) || !columns[col].validValueForSection(value) || !regions[reg].validValueForSection(value));
    }
    
    /*method to generate a sudoku puzzle from a valid sudoku board*/
    public void generateBlankSquares()
    {
        
        
        Random rand = new Random();//random number generator
        //places to store the randomly chosen indexes for a randomly choosen square
        int index1;
        int index2;
        
        boolean validSquareRemoval;
        
        int squaresBlankedOut = 0;
        
        while(squaresBlankedOut < 60)
        {
            //get random square
            index1 = rand.nextInt(sudokuSize);
            index2 = rand.nextInt(sudokuSize);
            
            board[index1][index2].setVisibility(false);//set it to be a blank square
            //make sure to reset solution values for every loop through of removing a square
            resetSolutionValues();
            
            //If we finally fill in all the Sudoku cells we return successfully
            validSquareRemoval = isSolvablePuzzle();
            
            
            //If we cannot fill in a cell, we return a fail condition, replace the number we tried to remove, 
            if(!validSquareRemoval)
            {
                board[index1][index2].setVisibility(true);
                
            }
            else
            {
                squaresBlankedOut++;
                
            }
                
            
        }
    }
    
    /*method to determine if there are blanksquare left in the solution puzzle
    pre:
    post:returns true if there are blank square left false if not*/
    public boolean blankSolutionSquaresLeft()
    {
        for(int r = 0; r < sudokuSize; r++)
        {
            for(int c = 0; c < sudokuSize; c++)
            {
                if(board[r][c].getSolutionValue() == 0)
                    return true;
            }
        }
        return false;
    }
        
    /*https://www.emogic.com/notes/programming_sudoku_algorithm_part_2_removing_numbers_board
    method that contain a simple sudoku solver
    pre:
    post: returns true if this puzzle can be solved using provided solving strategies false if not*/
    public boolean isSolvablePuzzle()
    {
        //booleans to store the return values of each solving strategy method
        boolean nakedSingles = true;
        boolean hiddenSingles = true;
        
        boolean blankSquaresLeft = true;
        
        //while ( ($blanksquaresleft == 1) and ($solvable == 1) ) {keep trying to solve the Sudoku grid}
        
        while(blankSquaresLeft && (nakedSingles || hiddenSingles))
        {
            //If, during the last loop, we did not fill in any blank squares and *there are blank square left*, this Sudoku grid is not solvable, 
            //so quit and return our failure.
            nakedSingles = nakedSinglesSolved();
            hiddenSingles = hiddenSinglesSolved();

            blankSquaresLeft = blankSolutionSquaresLeft();//see if there are blank squares left
        }
        //if we did not fill in any squares last go around and there are still blank squares
        //the board is not solvable so return a failure
        if(blankSquaresLeft)
            return false;

        //Obviously if there are no blank squares left in our Sudoku’s 9 x 9 grid,
        //then our Sudoku grid is solvable as we have filled it in. Return a success code.
        //** in my case I also decided to check if the solution values are the same as
        //the generated values to determine that each sudoku puzzle generated only has one 
        //valid solution making it a proper board
        
        if(!isSolvableCorrectlySolved())
            return false;
        
        
        return true;
    }
    
    /*method to determine if the solver found the same solution as the original board
    pre
    post: returns true if it found the same board false if not*/
    public boolean isSolvableCorrectlySolved()
    {
        for(int r = 0; r < sudokuSize; r++)
        {
            for(int c = 0; c < sudokuSize; c++)
            {
                if(!board[r][c].solvedCorrectly())
                    return false;
            }
        }
        return true;
    }
    
    /*method to clear the whole board*/
    public void clearBoard()
    {
        for(int r = 0; r < 9; r++)
        {
            for(int c = 0; c < 9; c++)
            {
                board[r][c].setValue(0);
                board[r][c].setSolutionValue(0);
            }
        }
    }
    
    /*method to clear just the solution board*/
    public void clearSolutionValues()
    {
        for(int r = 0; r < sudokuSize; r++)
        {
            for(int c = 0; c < sudokuSize; c++)
            {
                board[r][c].setSolutionValue(0);
            }
        }
    }
      
    /*method to reset the solution values based off of square visibility*/
    public void resetSolutionValues()
    {
        for(int r = 0; r < sudokuSize; r++)
        {
            for(int c = 0; c < sudokuSize; c++)
            {
                if(!board[r][c].getVisibility())//if it is supposed to be a blank square
                    board[r][c].setSolutionValue(0);//set value to 0
                else//else set value to the generated board value
                    board[r][c].setSolutionValue(board[r][c].getValue());
            }
        }
    }
    
    /*method to initialize the rows and columns arrays with the appropriate squares*/
    public void initializeRowsAndColumns()
    {
        for(int r = 0; r < 9; r++)
        {
            for(int c = 0; c < 9; c++)
            {
            
                rows[r].addSquare(board[r][c]);
                columns[c].addSquare(board[r][c]);
                
            }
        }
    }
    
    /*method to intialize the squares and regions of a sudoku game
    pre:
    post: initalizes each squares and assigns it to the appropriate region*/
    public void initializeSquaresAndRegions()
    {
        //for rows 0-2
        for(int r = 0; r < 3; r++)
        {
            //for columns 0-2
            for(int c = 0; c < 3; c++)
            {
                board[r][c] = new Square(r, c, 0);
                regions[0].getSquares()[r][c] = board[r][c];//assign to region
            }
            for(int c = 3; c < 6; c++)//for columns 3-5
            {
                board[r][c] = new Square(r, c, 1);
                regions[1].getSquares()[r][c - 3] = board[r][c];//assign to region
            }
            for(int c = 6; c < 9; c++)//for columns 5-8
            {
                board[r][c] = new Square(r, c, 2);
                regions[2].getSquares()[r][c - 6] = board[r][c];//assign to region
            }
        }
        for(int r = 3; r < 6; r++)//for rows 3-5
        {
            for(int c = 0; c < 3; c++)//for columns 0-2
            {
                board[r][c] = new Square(r, c, 3);
                regions[3].getSquares()[r - 3][c] = board[r][c];//assign to region
            }
            for(int c = 3; c < 6; c++)//for columns 3-5
            {
                board[r][c] = new Square(r, c, 4);
                regions[4].getSquares()[r - 3][c - 3] = board[r][c];//assign to region
            }
            for(int c = 6; c < 9; c++)//for columns 6-8
            {
                board[r][c] = new Square(r, c, 5);
                regions[5].getSquares()[r - 3][c - 6] = board[r][c];//assign to region
            }
        }
        for(int r = 6; r < 9; r++)//for rows 6-8
        {
            for(int c = 0; c < 3; c++)//for columns 0-2
            {
                board[r][c] = new Square(r, c, 6);
                regions[6].getSquares()[r - 6][c] = board[r][c];//assign to region
            }
            for(int c = 3; c < 6; c++)//for columns 3-5
            {
                board[r][c] = new Square(r, c, 7);
                regions[7].getSquares()[r - 6][c - 3] = board[r][c];//assign to region
            }
            for(int c = 6; c < 9; c++)//for columns 6-8
            {
                board[r][c] = new Square(r, c, 8);
                regions[8].getSquares()[r - 6][c - 6] = board[r][c];//assign to region
            }
        } 
    }
    
    
}
