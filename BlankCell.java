
package programmingproject7;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;

/*class for a blank sudoku cell that is a specailized type of jtextfield
 */
public class BlankCell extends JTextField
{
    Square sudokuSquare;//the square that is being represented by this cell
    
    /*construtor
    pre: takes in the square
    post: creates a blank sudoku cell to be solved by the user*/
    public BlankCell(Square square)
    {
        super();
        
        sudokuSquare = square;
        
        this.setText("");//set the text to nothing
        this.setEditable(false);//do not make it editable
        this.setHorizontalAlignment(JTextField.CENTER);//center the text
        this.setBackground(Color.WHITE);//set the background to white
        this.setForeground(Color.BLACK);//set the text to black
        
        this.setFont(new Font(Font.SERIF, Font.PLAIN, 20));//set font
        
    }
    
    
            
}
