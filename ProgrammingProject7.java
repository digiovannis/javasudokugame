
package programmingproject7;

import javax.swing.JFrame;

/**
* ProgrammingProject7.java
* Author: Shaila DiGiovanni
* Date: 12/13/2016
* Course: CSC2620
* Description: this program runs an easy sudoku game. The program will randomly 
* generate a new sudoku board and a puzzle with a unique solution every time it boots
* up or new game button is pressed. There is also a button to check the solution
* you put in.
*/
public class ProgrammingProject7 
{

    
    public static void main(String[] args) 
    {
        //create a new sudoku
        Sudoku board = new Sudoku();
        
        //set default close
        board.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Size of the window, in pixels
        board.setSize(1080, 540);

        // Make the window "visible"
        board.setVisible(true);
        
          
    }
    
}
