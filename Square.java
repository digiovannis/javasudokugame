
package programmingproject7;

import java.util.ArrayList;

/*class for a square in a sudoku grid
 */
public class Square 
{
    private int value;//the value in the solved grid
    private int solutionValue;//value used in testing if a square can be removed. this is the value that the solver comes up with
    private boolean visible;//if it is a blank square to be solved or if the value is shown
    private int rowNum;//row position in grid
    private int columnNum;//column position in grid
    private int regionNum;//region position in grid
    private ArrayList<Integer> possibleValues;//possible values the square could be used in generating a valid sudoku board
    private ArrayList<Integer> possibleSolutionValues;//possible values the square could be used in the solver to determine blank sqauares
    
    
    /*constructor
    pre: takes in three int for the row number column number and region number in the grid
    post: creates a square object*/
    public Square(int row, int column, int region)
    {
        value = 0;//set initial value to 0 to indicate not filled
        //assign row column and region numbers
        rowNum = row;
        columnNum = column;
        regionNum = region;
        //initalize arraylists
        possibleValues = new ArrayList<>();
        possibleSolutionValues = new ArrayList<>();
        //fill possible values with 1-9
        for(int i = 1; i < 10; i++)
        {
            possibleValues.add(i);
        }
        visible = true;//set visible to true to start
        solutionValue = 0;
        
    }
    
    /*method to remove a number from the possible valid solution values
    pre: takes in an int with the value to remove
    post: removes that number from the possible solution values arraylist*/
    public void removePossibleSolutionValue(int val)
    {
        int index = possibleSolutionValues.indexOf(val);//find the index of the value to remove
        if(index != -1)//if that value is in the arraylist
        {
            possibleSolutionValues.remove(index);//remove it
        }
    }
    
    /*method to determine if the solver correctly solved the square
    pre:
    post: returns true of the solution value and actual board value are the same
    returns false if they do not match*/
    public boolean solvedCorrectly()
    {
        if(value == solutionValue)
            return true;
        else
            return false;
    }
    
    /*method to get the arraylist containg the possible solution values
    pre:
    post: returns possibleSolutionValues*/
    public ArrayList<Integer> getPossibleSolutionValues()
    {
        return possibleSolutionValues;
    }
    
    /*method that sets the value of the square
    pre: takes in an int with the number the value is going to be set to
    post: value is set to val*/
    public void setValue(int val)
    {
        value = val;
    }
    
    /*method that sets the solution value of the square
    pre: takes in an int with the number the solution value is going to be set to
    post: solution value is set to val*/
    public void setSolutionValue(int val)
    {
        solutionValue = val;
    }
    
    /*method to reset the possible values for a square
    pre:
    post: possible values arraylist now contains 1-9*/
    public void replenishPossibleValues()
    {
        possibleValues = new ArrayList<>();
        for(int i = 1; i < 10; i++)
        {
            possibleValues.add(i);
        }
    }
    
    /*method that gets the possible values arraylist
    pre
    post: returns possibleValues*/
    public ArrayList<Integer> getPossibleValues()
    {
        return possibleValues;
    }
    
    /*get method for the region of the square
    pre
    post: returns the region number*/
    public int getRegion()
    {
        return regionNum;
    }
    
    /*get method for the column of the square
    pre
    post: returns the column number*/
    public int getColumn()
    {
        return columnNum;
    }
    
    /*get method for the row of the square
    pre
    post: returns the row number*/
    public int getRow()
    {
        return rowNum;
    }
    
    /*get method for the value of the square
    pre
    post: returns the value*/
    public int getValue()
    {
        return value;
    }
    
    /*get method for the solution value of the square
    pre
    post: returns the solution value*/
    public int getSolutionValue()
    {
        return solutionValue;
    }
    
    /*method that sets the visibility of the square
    pre: takes a boolean for if the square should be visible
    post: sets the visibility and the solution value to 0 if not visible
    or to the value of the square if it it visible*/
    public void setVisibility(boolean vis)
    {
        visible = vis;
        if(!visible)
            solutionValue = 0;
        else
            solutionValue = value;
    }
    
    /*get method for the visibility of the square
    pre
    post: returns the visibility*/
    public boolean getVisibility()
    {
        return visible;
    }
    
    
}
