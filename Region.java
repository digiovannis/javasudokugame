
package programmingproject7;

import java.util.ArrayList;

/**class for a 3x3 region in a sudoku board
 */
public class Region implements Section
{
    //private int regionNum;
    private Square[][] squares;
    
    public Region()
    {
        squares = new Square[3][3];
    }
    
    /*method to find if a there is a value in the section that only appears in 1
    square's possible solutions
    pre: assumes there is a hidden single(determined in a different method) 
    also takes in the value that is the hidden single
    post: return the square that contains the hidden single number*/
    public Square getHiddenSingle(int value)
    {
        Square hiddenSingle;//square to return
        int index;//index of the value
        
        for(int r = 0; r < squares.length; r++)
        {
            for(int c = 0; c < squares.length; c++)
            {
                //if this square's possible solution values contain the hidden single number
                index = squares[r][c].getPossibleSolutionValues().indexOf(value);
                if(index != -1)
                {
                    hiddenSingle = squares[r][c];
                    return hiddenSingle;//return that square
                }
            }
            
        }
        
        return null;
    }
    
    /*method that counts the number of times 1-9 is a possible solution in each square
    for the section
    pre:
    post: returns an int array of size 9 with each index holding the number of times
    its index+1 appears in the possible soltutions*/
    public int[] countPossibilitiesOfSolutionValues()
    {
        int[] numberOfPossibilities = new int[9];//array to return
        
        ArrayList<Integer> possibleValues;
        
        //for each square
        for(int r = 0; r < squares.length; r++)
        {
            for(int c = 0; c < squares.length; c++)
            {
                if(!squares[r][c].getVisibility())
                {
                    possibleValues = squares[r][c].getPossibleSolutionValues();//store this squares possible solutionvalues
            
                    //for each possible value
                    for(int j = 0; j < possibleValues.size(); j++)
                    {
                        int value = possibleValues.get(j);
                        numberOfPossibilities[value-1]++;//update appropriate array location count
                    }
                }
                
            }
            
        }
        
        
        return numberOfPossibilities;
    }
    
    /*get method for the squares array
    pre
    post: returns the squares array*/
    public Square[][] getSquares()
    {
        return squares;
    }
    
   

    /*method to test if a value is already in this section
    pre: takes in the value to test for
    post: returns false if the value is already in the section
    returns true if it is not*/
    @Override
    public boolean validValueForSection(int value) 
    {
        for(int i = 0; i < 3; i++)
        {
            for(int j = 0; j < 3; j++)
            {
                if(value == squares[i][j].getValue())
                    return false;
            }
            
        }
        
        return true;
    }

    /*method to test if a value is already in this solution section
    pre: takes in the value to test for
    post: returns false if the value is already in the section of the solution
    returns true if it is not*/
    @Override
    public boolean validValueForSolutionSection(int value) 
    {
        for(int i = 0; i < 3; i++)
        {
            for(int j = 0; j < 3; j++)
            {
                if(value == squares[i][j].getSolutionValue())
                    return false;
            }
            
        }
        
        return true;
    }
}
