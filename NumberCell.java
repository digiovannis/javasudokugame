
package programmingproject7;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;

/*class for a filled in sudoku cell that is a specailized type of jtextfield
 */
public class NumberCell extends JTextField
{
    Square sudokuSquare;//the square that is being displayed in this cell
    
    /*construtor
    pre: takes in the square
    post: creates a sudoku cell that displays a fixed number*/
    public NumberCell(Square square)
    {
        super();
        
        sudokuSquare = square;
        
        
        this.setText(Integer.toString(sudokuSquare.getValue()));//set the text to the squares value
        this.setEditable(false);//do not make it editable
        this.setHorizontalAlignment(JTextField.CENTER);//set the text to be in the center
        this.setBackground(Color.LIGHT_GRAY);//color it gray to distinguish it from blank cells
        
        this.setFont(new Font(Font.SERIF, Font.PLAIN, 20));//set the font
        this.setForeground(Color.BLACK);//set text to black

    }
    
    
}
