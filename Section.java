/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programmingproject7;

/**
 *
 * @author digio
 */
public interface Section 
{
    public boolean validValueForSection(int value);
    
    public boolean validValueForSolutionSection(int value);
}
